import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: () =>
      import(/* webpackChunkName: "home" */ '../components/Home.vue'),
  },
  {
    path: '/create-char',
    name: 'create-char',
    component: () =>
      import(
        /* webpackChunkName: "create-char" */ '../pages/CreateCharacter.vue'
      ),
  },
  {
    path: '/list-chars',
    name: 'list-chars',
    component: () =>
      import(
        /* webpackChunkName: "list-chars" */ '../pages/ListAllCharacters.vue'
      ),
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
  hash: false,
});

export default router;
