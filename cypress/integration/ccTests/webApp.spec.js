describe('All Round Testing', () => {
  before(() => {
    cy.wait(1500);
    cy.visit('http://localhost:8080/');
    cy.wait(1500);
  });

  it('should find all elements on the home page', () => {
    cy.get('#app')
      .eq(0)
      .within(() => {
        cy.get('#container')
          .eq(0)
          .within(() => {
            cy.get('.text-center').eq(0);
            cy.get('#heading')
              .eq(0)
             .should('contain', 'Welcome to Character Companion');
            cy.get('#sub-heading')
              .eq(0)
              .should(
                'contain',
                'A simple web application to help you build your RPG character.'
              );
            cy.get('#btn-row').eq(0);

            cy.get('#btn-create')
              .eq(0)
              .should('contain', 'Create Character')
              .click();
          });
      });
  });
  it('should create a character', () => {
    cy.get('#create-char')
      .eq(0)
      .within(() => {
        cy.get('#form')
          .eq(0)
          .within(() => {
            cy.get('#container')
              .eq(0)
              .within(() => {
                cy.get('#h1-heading')
                  .eq(0)
                  .should('contain', 'Character Creation');
                cy.get('#jumbo').eq(0);
                cy.get('#char-label')
                  .eq(0)
                  .should('contain', 'Character Name');
                cy.get('#input-with-list-name')
                  .eq(0)
                  .clear()
                  .type('Donal De Donky');
                cy.get('#race-label')
                  .eq(0)
                  .should('contain', 'Race');
                cy.get('#race-select')
                  .eq(0)
                  .select('Human');
                cy.get('#align-label')
                  .eq(0)
                  .should('contain', 'Alignment');
                cy.get('#align-form')
                  .eq(0)
                  .select('Neutral');
                cy.get('#gender-label')
                  .eq(0)
                  .should('contain', 'Gender');
                cy.get('#gender-select')
                  .eq(0)
                  .select('Male');
                cy.get('#backstory-label')
                  .eq(0)
                  .should('contain', 'Back Story');
                cy.get('#backstory')
                  .eq(0)
                  .clear()
                  .type(
                    'Donal De Donky is my name and playing RPGs is my game.'
                  );
                  cy.get('#p-strength')
                  .eq(0)
                  .should('contain', 'Strength: ');
                cy.get('#strength-rating')
                  .eq(0)
                  .click(6);
                cy.get('#p-dex')
                  .eq(0)
                  .should('contain', 'Dexterity: ');
                cy.get('#dex-rating')
                  .eq(0)
                  .click(6);
                cy.get('#p-cons')
                  .eq(0)
                  .should('contain', 'Constitution: ');
                cy.get('#const-rating')
                  .eq(0)
                  .click(6);
                cy.get('#p-wisdom')
                  .eq(0)
                  .should('contain', 'Wisdom: ');
                cy.get('#wisdom-form')
                  .eq(0)
                  .click(6);
                cy.get('#p-char')
                  .eq(0)
                  .should('contain', 'Charisma: ');
                cy.get('#char-rating')
                  .eq(0)
                  .click(6);
                cy.get('#p-intel')
                  .eq(0)
                  .should('contain', 'Intelligence: ');
                cy.get('#intel-rating')
                  .eq(0)
                  .click(6);
                cy.get('#create-btn')
                  .eq(0)
                  .click();
              });
          });
      });
  });
  it('should list all characters', () => {
    cy.get('#btn-list')
      .eq(0)
      .click();
    cy.wait(1500);
    cy.get('#table-1');
    cy.get('#btn-delete').eq(0);
    cy.get('#nav-brand').click();
  });
});