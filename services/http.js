import store from '../services/store';
import axios from 'axios';
//const error = 'Critical error in http.js';

export function http() {
  try {
    if (process.env.NODE_ENV === 'development') {
      return axios.create({
        baseURL: store.state.apiUrlStaging,
        
      });
    }
    if (process.env.NODE_ENV === 'production') {
      return axios.create({
        baseURL: store.state.apiUrlProduction,
      });
    }
    if (process.env.NODE_ENV === 'local') {
      return axios.create({
        baseURL: store.state.apiUrlLocal,
      });
    }
  } catch (error) {
    console.log(error);
  }
}
