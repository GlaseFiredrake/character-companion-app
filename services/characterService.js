import { http } from './http';

// Get all characters
export async function getAllCharacters() {
  return http()
    .get(`api/character`, {
      method: 'GET',
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      return error;
    });
}

// Get an individual character
export async function getCharacter(id) {
  return http()
    .get(`api/character/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      return error;
    });
}

// Create a character
export async function createCharacter(character) {
  return http()
    .post('api/character', character)
    .catch((error) => {
      return error;
    });
}

// Delete a character
export function deleteCharacter(id) {
  return http()
    .delete(`/api/character/${id}`)
    .catch((error) => {
      return error;
    });
}

// Update a character
export function updateCharacter(id, character) {
  return http()
    .put(`/order/update/${id}`, character)
    .catch((error) => {
      return error;
    });
}

// End
