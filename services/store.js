import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    apiUrlProduction: 'https://character-companion-prod.herokuapp.com/',
    apiUrlStaging: 'https://character-companion-dev.herokuapp.com/',
    apiUrlLocal: `http://localhost:5000/`,
  },
});

export default store;
